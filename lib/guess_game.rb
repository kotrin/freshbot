require 'nokogiri'
require 'rest_client'

RANDOM_URL = "http://www.urbandictionary.com/random.php"

class WordGetter
  def self.get_word
    random_page = (2..800).to_a.sample
    new_url = RANDOM_URL + "?page=#{random_page}"
    req = RestClient.get(new_url)
    WordGetter.from_html(req.body)
  end

  def self.from_html(html)
    doc = Nokogiri.HTML(html)
    words = doc.css('.word')
    return nil if words.empty?
    amount = words.size
    the_word = rand(amount)
    word = words[the_word].content.strip
    word
  end
end

class GuessGame
  attr_reader :word

  def initialize
    @word = get_word!
    @word_history = []
    set_current_guess
  end

  def get_word!
    WordGetter.get_word
  end

  def guess(char)
    if char.size == 1
      guess_locations = []
      @word.split('').each_with_index do |c, i|
        if char.downcase == c.downcase
          guess_locations.push(i)
        end
      end
      guess_locations.each do |i|
        @current_guess[i] = @word[i]
      end
      if done?
        msg = "you won the game! the word was: #{@word}\n\ntime for a new game!\n\n"
        msg += new_game!
        msg
      else
        "your current progress: `#{print_current_guess.split('').join(' ')}`"
      end
    elsif char.size == @word.size
      if char.downcase == @word.downcase
        msg = "holy tits! you guessed the whole word. well done!\n\n"
        msg += new_game!
        msg
      else
        fun_word = %w|turd dork idiot smarty butthole jon-like-idiot farter twink dink snorter snotface|.sample
        "wrong. try again, #{fun_word}"
      end
    else
      "you can only guess 1 character at a time!"
    end
  end

  def new_game!
    @word_history.push(@word)
    new_word = get_word!
    while @word_history.include?(new_word)
      new_word = get_word!
    end
    @word = new_word
    if @word_history.size > 80
      @word_history.delete_at(rand(80))
    end
    @current_guess = Array.new(@word.size)
    "the new word has #{@word.size} characters."
  end

  # def new_word!
  #   old_word = @word
  #   @word = UrbanDictionary.random_word
  #   "the old word was: #{old_word}. \n\nthe new word has #{@word.size} characters."
  # end

  def set_current_guess
    @current_guess = Array.new(@word.size)
    @word.split('').each_with_index do |c,i|
      if c == ' '
        @current_guess[i] = ' '
      end
    end
  end

  def print_current_guess
    @current_guess.map do |w|
      if w.nil?
        "_"
      else
        w
      end
    end.join("")
  end

  def done?
    !@current_guess.any?(&:nil?)
  end
end
