# freshbot game new
# freshbot game guess 123
# freshbot game end

class GamePool
  attr_reader :game_pool, :game_history

  def initialize
    @game_pool = []
    @game_history = GameHistoryPool.new
  end

  def new_game(user:)
    old_game = find_game(user)
    if old_game
      remove_game(old_game)
    end
    new_game = Game.new(user: user)
    add_game(new_game)
    "the game has started!"
  end

  def end_game(user:)
    game = find_game(user)
    if game
      remove_game(game)
      "game removed."
    else
      "there was no game to end."
    end
  end

  def commit_guess(user:, guess:)
    game = find_game(user)
    if game
      msg = game.guess(guess)
      if game.over?
        @game_history.add_game(user: user, win: game.win?)
        remove_game(game)
      end
      return msg
    else
      return "you have not started a game!"
    end
  end

  def find_game(user)
    @game_pool.find do |g|
      g.user == user
    end
  end

  def remove_game(game)
    @game_pool.delete(game)
  end

  def add_game(game)
    @game_pool.push(game)
  end

  def history_msg
    @game_history.history_msg
  end
end

class Game
  attr_reader :user

  def initialize(user:)
    @user = user
    @number = rand(100)
    @guesses = 5
    @win = false
  end

  def guess(n)
    @guesses -= 1
    if n < @number
      with_guesses "too low"
    elsif n > @number
      with_guesses "too high"
    elsif n == @number
      @win = true
      @guesses = 0
      "just right! you won!"
    else
      "something totally fucked up happened and i don't remember the number"
    end
  end

  def with_guesses(msg)
    if @guesses > 0
      msg + " -- guesses remaining: #{@guesses}"
    else
      msg + ". that was your last guess. you lose. the number was: #{@number}"
    end
  end

  def over?
    win? || @guesses <= 0
  end

  def win?
    @win
  end
end

class GameHistoryPool
  def initialize
    @game_history_pool = []
  end

  def add_game(user:,win:)
    history = find_history(user)
    if history
      history.add_game(win)
    else
      new_history = GameHistory.new(user: user)
      new_history.add_game(win)
      add_history(new_history)
    end
  end

  def add_history(history)
    @game_history_pool.push(history)
  end

  def find_history(user)
    @game_history_pool.find do |history|
      history.user == user
    end
  end

  def history_msg
    users = @game_history_pool.map(&:to_s).join("\n")
    total_games = @game_history_pool.map(&:game_count).sum
    total_wins = @game_history_pool.map(&:win_count).sum
    stat_line = "Total Games: #{total_games} | Total Wins: #{total_wins}"
    heading = "\nMy recent memory has...\n\n"

    "#{heading}#{users}\n\n#{stat_line}\n"
  end
end

class GameHistory
  attr_reader :user, :win_count, :game_count

  def initialize(user:)
    @user = user
    @game_count = 0
    @win_count = 0
  end

  def add_game(win)
    if win == true
      @win_count += 1
    end
    @game_count += 1
  end

  def to_s
    "<@#{@user}> | Win: #{@win_count} | Loss: #{loss}"
  end

  def loss
    @game_count - @win_count
  end
end
