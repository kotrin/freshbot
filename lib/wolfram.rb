class Wolfram
  def initialize(client_connection, question)
    @client_connection = client_connection
    @question = question
    @client_response = nil
  end

  def answer
    question_answer = ""

    begin
      @client_response = @client_connection.query(@question)

      result = find_pod_by(:title, "Result")

      if result
        question_answer = result.subpods[0].plaintext
      else
        interp = find_pod_by(:title, "Input interpretation")
        notes = find_pod_by(:title, "Notes")

        question_answer = "#{interp.subpods[0].plaintext}\n" if interp
        if notes
          question_answer += "#{notes.subpods[0].plaintext}"
        else
          data_second = find_pod_by(:position, 200)
          question_answer += "#{data_second.subpods[0].plaintext}" if data_second
        end
      end

    rescue => error
      puts "====== WOLFRAM FAIL:"
      puts "question: #{@question}"
      puts error
    end

    if question_answer.strip.empty? || question_answer =~ /\A\s*\z/
      "Uh...I have no idea."
    else
      question_answer
    end
  end

  private

  def find_pod_by(attr, match_item)
    @client_response.find { |pod| pod.send(attr) == match_item }
  end


end
