require 'slack-ruby-bot'
require 'wolfram-alpha'
require 'urban_dictionary'
require 'rest-client'

require_relative 'eliza'
require_relative 'wolfram'
require_relative 'games'
require_relative 'guess_game'

SlackRubyBot::Client.logger.level = Logger::WARN



class FreshBot < SlackRubyBot::Bot
  @@eliza = Eliza.new
  @@wolfram = WolframAlpha::Client.new(ENV['WOLFRAM_APPID'], { "format" => "plaintext" })
  @@games = GamePool.new
  @@word_game = GuessGame.new

  command 'ping' do |client, data, match|
    client.say(text: 'pong', channel: data.channel)
  end

  command 'game new' do |client, data, match|
    msg = @@games.new_game(user: data.user)
    client.say(text: "<@#{data.user}> #{msg}", channel: data.channel)
  end

  command 'game end' do |client, data, match|
    msg = @@games.end_game(user: data.user)
    client.say(text: "<@#{data.user}> #{msg}", channel: data.channel)
  end

  match /^freshbot game guess (?<number>.+)/i do |client, data, match|
    number = match[:number].to_i
    msg = @@games.commit_guess(user: data.user, guess: number)
    client.say(text: "<@#{data.user}> #{msg}", channel: data.channel)
  end

  match /^freshbot wordgame guess (?<guess>.+)/i do |client, data, match|
    msg = @@word_game.guess(match[:guess])
    client.say(text: "<@#{data.user}> #{msg}", channel: data.channel)
  end

  command 'game history' do |client, data, match|
    msg = @@games.history_msg
    client.say(text: "<@#{data.user}> #{msg}", channel: data.channel)
  end

  command 'whoami' do |client, data, match|
    client.say(text: "<@#{data.user}> #{data.user}", channel: data.channel)
  end

  match /tell me what’s fresh/i do |client, data, match|
    client.say(text: "YO <@#{data.user}>, I'll tell you what's fresh... :poop:", channel: data.channel)
  end

  match /^freshbot define (?<lookup>.+)/i do |client, data, match|
    response = ""
    lookup = match[:lookup]
    begin
      word = UrbanDictionary.define(lookup).entries.first
      response = "\n#{word.definition}\n#{word.example}"
    rescue => e
      puts "====== URBANDICTIONARY FAIL:"
      puts "lookup: #{lookup}"
      puts e
    end
    response = if response.strip.empty? || response =~ /\A\s*\z/
      ["IDK MY BFF JILL?", "Ur Butt?", "LOL FUK OFF BRO I DON'T KNOW IT ALL", "hmmm.... let me think... ok nah i don't know", "yo bro, i can't figure it out. i'm sorry dawg."].sample
    else
      response
    end
    client.say(text: "<@#{data.user}> #{response}", channel: data.channel)
  end

  match /freshbot what is (?<question>.+)/i do |client, data, match|
    if match[:question] =~ /\[.*\]/ || match[:question] =~ /\d+\.\.\d+/
      client.say(text: "cody sucks and made me not want to answer that", channel: data.channel)
    else
      question_answer = Wolfram.new(@@wolfram, match[:question]).answer
      client.say(text: "<@#{data.user}> #{question_answer}", channel: data.channel)
    end
  end

  match /freshbot/i do |client, data, match|
    response = @@eliza.analyze(data.text.downcase.gsub("freshbot", "").strip)
    client.say(text: "<@#{data.user}> #{response}", channel: data.channel)
  end
end
